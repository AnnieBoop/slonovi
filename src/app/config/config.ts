export const config = {
    appName: 'My Awesome ZOO',
    apiUrl: 'http://localhost:3000',
    slonoviTableConfig: [
        { key: 'id', label: 'Id' },
        { key: 'ime', label: 'Ime' },
        { key: 'kilaza', label: 'Kilaza' },
        { key: 'visina', label: 'Visina' }
    ]
}