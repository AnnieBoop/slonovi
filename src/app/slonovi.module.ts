import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import {FormsModule} from '@angular/forms';


import { SlonoviComponent } from './slonovi.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';

import {MenuModel} from './models/menu.model';

import {SlonoviModel} from './models/slonovi.model';
import {SlonoviService} from './services/slonovi.service';
import { SlonComponent } from './routes/slon/slon.route';
import { DetaljiSlonaComponent } from './routes/detalji-slona/detalji-slona.route';
import { EditujSlonaComponent } from './routes/edituj-slona/edituj-slona.route';
import { NoviSlonComponent } from './routes/novi-slon/novi-slon.route';
import { HomeComponent } from './routes/home/home.route';
import { TableSlonovaComponent } from './routes/table-slonova/table-slonova.route';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';
import { FilterPipe } from './pipes/filter.pipe';
import {HighlightDirective} from './directives/highlight'

const routes:Routes = [
  {path:"",component:HomeComponent},
 {path:"dashboard",component:TableSlonovaComponent},
 {path:"slonovi/novi",component:NoviSlonComponent},
 {
  path: "slon/:id", component: SlonComponent, children: [
    { path: "edit", component: EditujSlonaComponent },
    { path: "info", component: DetaljiSlonaComponent },
  ]
},
]

@NgModule({
  declarations: [
    SlonoviComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    TableSlonovaComponent,
    NoviSlonComponent,
    SlonComponent,
    EditujSlonaComponent,
    DetaljiSlonaComponent,
    MenuLinkComponent,
    FilterPipe,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [
    MenuModel,
    SlonoviModel, 
    SlonoviService
  ],
  bootstrap: [SlonoviComponent]
})
export class SlonoviModule { }
