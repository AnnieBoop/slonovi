import {Injectable} from '@angular/core' ;
import {SlonoviService} from '../services/slonovi.service';

@Injectable()
export class SlonoviModel{

    slonovi = [];

    constructor(private service: SlonoviService) {
        this.refreshujSlonove(()=>{});
    }

    refreshujSlonove(clbk) {
        this.service.getAll().subscribe(
            (slonovi) => {
                this.slonovi = slonovi;
                clbk()
            }
        );
    }

    updejtujSlona(slon){
        this.service.update(slon).subscribe((updejtovaniSlon)=>{
            this.refreshujSlonove(()=>{})
        })
    }

    getSlonaById(id, clbk){
        this.service.getById(id).subscribe(clbk);
    }

    getSlonaByIdCallback(id, clbk){
        if(this.slonovi.length < 1){
            this.refreshujSlonove(()=>{
                for (let i = 0; i < this.slonovi.length; i++) {
                    if (id == this.slonovi[i].id) {
                        clbk(this.slonovi[i])
                    }
                }
            })
        }else{
            for (let i = 0; i < this.slonovi.length; i++) {
                if (id == this.slonovi[i].id) {
                    clbk(this.slonovi[i])
                }
            }
        }
        
    }

    dodajSlona(slon) {
        this.service.post(slon).subscribe(
            (slon) => {
                this.slonovi.push(slon);
            }
        )
    }

    dajMiIndexSlonaPoIdju(id,clbk) {
        for (let i = 0; i < this.slonovi.length; i++) {
            if (id == this.slonovi[i].id) {
                clbk(i)
            }
        }
    }

    obrisiSlona(id) {
        this.service.delete(id).subscribe(() => {
           this.dajMiIndexSlonaPoIdju(id,(index)=>{
                this.slonovi.splice(index,1);
            });
           
        });
    }

}