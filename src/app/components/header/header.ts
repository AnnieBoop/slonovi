import { Component, OnInit } from '@angular/core';
import { config } from '../../config/config';

@Component({
  selector: 'aheader',
  templateUrl: './header.html'
})
export class HeaderComponent implements OnInit {

  appName = config.appName;

  constructor() { }

  ngOnInit() {
  }

}
