import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'slon',
  templateUrl: './slon.route.html'
})
export class SlonComponent implements OnInit {

  id;

  constructor(private router:Router,private route:ActivatedRoute ) {
    this.route.params.subscribe(({id})=>{
      this.id = id;
    })
  }

  goToPath(subpath){
    this.router.navigate(['/slon',this.id,subpath])
  }

  ngOnInit() {
  }

}

