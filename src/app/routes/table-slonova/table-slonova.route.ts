import { Component, OnInit } from '@angular/core';
import { SlonoviModel } from '../../models/slonovi.model';
import { config } from '../../config/config';
import { Router } from '@angular/router';

@Component({
  selector: 'table-slonova',
  templateUrl: './table-slonova.route.html'
})
export class TableSlonovaComponent implements OnInit {

  searchString = "";
  conf = config.slonoviTableConfig;

  constructor(public model: SlonoviModel, private router: Router) { }

  ngOnInit() {
  }
  obrisiSlona(id) {
    this.model.obrisiSlona(id);
  }

  editujSlona(id) {
    this.router.navigate(['/slon',id,'edit'])
  }
}
