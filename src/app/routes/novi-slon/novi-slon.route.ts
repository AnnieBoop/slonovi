import { Component, OnInit } from '@angular/core';
import {SlonoviModel} from '../../models/slonovi.model'

@Component({
 selector: 'novi-slon',
 templateUrl: './novi-slon.route.html'
})
export class NoviSlonComponent implements OnInit {

    noviSlon = {
        ime:null,
        kilaza:null,
        visina: null
      };
    
      constructor(public model:SlonoviModel) { }
    
      dodajSlona(){
        if(this.noviSlon.ime && this.noviSlon.kilaza && this.noviSlon.visina){
          this.model.dodajSlona(this.noviSlon);
        }
        
      }
 ngOnInit() {
 }

}
